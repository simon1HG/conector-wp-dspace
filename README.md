# WP Biblioteca Digital

Plugin WP utilizado por el sitio web institucional para las búsquedas de contenidos en la "Biblioteca digital" https://bibliotecadigital.odepa.gob.cl y el sitio web institucional https://www.odepa.gob.cl


# Conector Wordpress/Dspace

El conector permite conectar y consumir información desde un Dspace a wordpress, 
esta información son las publicaciones con los metadatos y referencia a archivos. 
A su vez se podrá consumir el esquema de metadatos en dublin core (dc). 

Esta información se almacena dentro de 5 tablas dentro de la Base de Datos de wordpress, 
creadas al momento de activar el conector. 
Estas son independiente a las tablas creadas por wordpress, por lo que no influyen en el esquema principal de la base de datos.

Este consumo de información lo hace a través de la API Rest que dispone Dspace. 
En este caso se ocupan: 

Api Rest: https://bibliotecadigital.odepa.gob.cl/rest

Items: https://bibliotecadigital.odepa.gob.cl/rest/items

Esquema de metadatos: https://bibliotecadigital.odepa.gob.cl/rest/registries/schema/dc

La API Rest se consume con una funcion en PHP llamada "file_get_contents" en formato json.

##Plugin 
El plugin se muestra solamente a los usuarios administradores y Super-administradores

## Funciones:

El conector actualmente cuenta con 5 funciones, de las cuales 3 son para el manejo de la información y las otras 2 son para el registro de la fuente de datos y el esquema de metadatos.

### Insert:
Insert Schema y Fuente de datos: 

#### Requisitos:

url de fuente de datos.
Nombre de fuente de datos.

#### Objetivo: 
Esta función debe ser la primera en ejecutarse al instalar el plugin, solo es necesario ejecutarla solo una vez. Este evento registra la fuente de datos y el esquema de metadatos de la misma. si no se ejecuta esta función antes de registrar las publicaciones por primera vez, dará un error. 

### Insert Items desde Dspace: 

#### Requisitos:

Url de fuente de datos.

Tener registrado esquema de metadatos..

#### Objetivo: 

Leer los items desde Dspace y registrarlo en la base de datos del plugin.

##### Observación: 
De no ser la primera vez que se ejecuta, la función verifica si el ítem se encuentra ya registrado, en el caso de ser así se omite y no se registra, de lo contrario el ítem será ingresado en la base de datos.


### Update Metadatos de los Items:
#### Requisitos:
Haber realizado ambos insert anteriores
#### Objetivo:
Actualizar la información del ítem, la función hace una revisión a la fecha.

### Delete Items:
#### Requisitos: 
Haber realizado ambos insert anteriormente
#### Objetivo: 
Automatizar la eliminación de ítems que ya no se encuentra en Dspace.
   


##  Instalación y Activación

### Pre-requisitos
Wordpress -> Version: 5.2 minimo.

PHP -> Version: 7 minimo.

MySql -> Version: 5 minimo.

### Instalación

Para instalar el plugin, se debe hacer un clone del proyecto 

```bash
git clone http://gitlab.odepa.local/desarrollo/externos/prodigio-consultores/wp-biblioteca-digital.git
```

a la carpeta de plugins de Wordpress.

```
Ruta_wordpress\wp-content\plugins
```

### Activación del plugin
En wordpress, en la pestaña de Plugins, aparecerá el Conector con el nombre 'WP-Biblioteca Digital connect'
y solamente hay que darle a activar.


Al activar el plugin, en la base de datos se crearán 5 tablas nuevas con un prefijo 'wp_wpdsc', que no influyen el esquema original
de la base de datos. 

#### NOTA: Al desactivar el plugin estas tablas se eliminan las tablas y toda la información en esta.

Además de crear las tablas, el plugin crea un Menú en el panel lateral de wordpress para poder hacer de su uso.


## Uso

### Primer paso
Ir a la pagina principal del plugin. Se encontrarán 2 campos que se deben llenar (el origen de los datos: https://bibliotecadigital.odepa.gob.cl
y el nombre del sitio: 'Biblioteca Digital de Odepa').

se actualizará la pagina y se mostrará los datos ingresados en una tabla con un botón para consumir e indexar los datos de la biblioteca en la base de datos, y un botón 
con el nombre 'Guardar' que sirve para actualizar los datos (url y nombre de la biblioteca). 

### “Mantenimiento” : 
El conector cuenta con  la opción de poder ejecutar las funciones anteriormente mencionadas de forma manual. En la página de mantenimiento se puede seleccionar qué función ejecutar y revisar la fuente de datos, cuando se ejecutan la pantalla comienza a cargar y ejecutar la función.

Observación:  puede que se quede cargando harto tiempo pero sigue ejecutando la función, e incluso se puede detener la carga de la página y aún así la función seguirá en curso.


### Cron jobs:
El conector además cuenta con la opción de crear una tarea programada para que la función se ejecute en una hora específica cada cierto tiempo que se estime conveniente.
En la página de Cron options tiene que indicar la fecha y hora de inicio en formato : YYYY-MM-DD HH:MM:SS, luego seleccionar el periodo en el cual se ejecutará la función, luego seleccionar qué función se va a dejar programada y finalmente indicar el origen de los datos.

##### Observación: su funcionamiento no ha sido probado completamente.

##### Recomendación: Para el uso y control de este plugin se recomienda instalar el plugin “WP Crontrol”, este plugin permite eliminar y ejecutar tareas programadas entre otras funciones.  link: https://wordpress.org/plugins/wp-crontrol/ 

### Base de datos WP: 
En la base de datos de wp se crean 5 tablas:

#### wp_wpdsc_origen

##### campos: id, s_nombre, s_url

Esta tabla guarda el nombre y la url del origen de los datos. su funcion es de almacenar estos datos para no volverlos a ingresar nuevamente cada vez que se requieras.

#### wp_wpdsc_metadataschema

##### campos: id, s_schema, s_element

Esta tabla guarda el esquema dublin core utilizado en Dspace, es importante guardar el esquema DC para que los metadatos se hagan mas facil de indentificar al momento de hacer una busqueda

#### wp_wpdsc_items

##### campos: id, s_handle, i_origen_id

Esta tabla guarda el handle y el id de la url de origen. Esto sirve para despues armar las urls de los items. tomando la url de origen y el handle agregandole la path /handle/ entre ambos campos

ejemplo: 
```sql
select wp_wpdsc_origen.s_url, wp_wpdsc_items.s_handle from wp_wpdsc_origen, wp_wpdsc_items where wp_wpdsc_items.i_origen_id = wp_wpdsc_origen.id

```

el resultado mostraria la url de origen y el handle, y para armar la url seria:
```php
wp_wpdsc_origen.s_url.'/handle/'.wp_wpdsc_items.s_handle
```

lo que quedaria: 

https://bibliotecadigital.odepa.gob.cl/handle/20.500.12650/879


#### wp_wpdsc_metadatavalue

##### campos: id, i_schema_id, i_items_id, s_text_value

Esta tabla guarda los valores de los metadatos el item, en el campo i_schema_id conecta con la tabla "wp_wpdsc_metadataschema" e indica a que metadato corresponde.

el campo i_items_id conecta con la tabla "wp_wpdsc_items" e indica a que item corresponde este metadato.

el campo s_text_value guarda el texto que corresponda al metadato, ejemplo si el metadato es dc.tite en este campo se guardaria "Boletín de carne bovina"


#### wp_wpdsc_bitstreams

##### campos: id, s_name, s_url, s_format, i_sizebytes, i_items_id, s_bundle

Esta tabla guarda la referencia a los archivos que contiene cada item.
en el campo s_name guarda el nombre del documento, en el campo s_url la url de referencia para descargar el archivo
en el campo s_format guarda el formato del archivo, ejemplo pdf. 

El campo i_sizebytes guarda el tamaño en bytes del archivo. El campo i_items_id conecta con la tabla "wp_wpdsc_items" e indica a que item corresponde este archivo. 

y por ultimo el campo s_bundle guarda que tipo de archivo es en dspace: 

ejemplo: 

Original -> archivos subidos al item, TEXT -> archivos de texto extraidos de los documentos originales, THUMBNAIL -> imagen o portada del item.