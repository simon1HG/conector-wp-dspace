<?php
/*
Plugin Name: WP Dspace connect
Plugin URI: https://prodigioconsultores.com
Description: Get and embed items data from a dspace repository in your wordpress through a REST API.
Version: 2.0
Author: Jordan Gamonal Perez
Author URI: https://prodigioconsultores.com
License: MIT
Text Domain: wp_dspace_connect
Domain Path: /languages
*/

	defined('ABSPATH') or die("Bye bye");

	define('WPDSC_ROUTE',plugin_dir_path(__FILE__));
	require_once(WPDSC_ROUTE . 'includes/admin_menu.php');
	require_once(WPDSC_ROUTE . 'install.php');
	require_once(WPDSC_ROUTE . 'uninstall.php');
	
	add_action('plugins_loaded', 'wpdsc_plugin_load_textdomain');

	function wpdsc_plugin_load_textdomain() {
		
		$text_domain	= 'wp_dspace_connect';
		$path_languages = basename(dirname(__FILE__)).'/languages/';

		load_plugin_textdomain($text_domain, false, $path_languages );
	}

	register_activation_hook( __FILE__,'wpdsc_install');
	register_deactivation_hook( __FILE__,'wpdsc_uninstall');
