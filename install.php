<?php
/*
	Creating tables to the plugin use.
*/

	function wpdsc_install() {
		global $wpdb;

		$table_name = $wpdb->prefix . 'wpdsc_metadataschema';

		if( $wpdb->get_var('SHOW TABLES LIKE ' . $table_name) != $table_name ) {

			$charset_collate = $wpdb->get_charset_collate();

			$sql = "CREATE TABLE {$wpdb->prefix}wpdsc_metadataschema (
				id bigint(20) NOT NULL AUTO_INCREMENT,
				s_schema varchar(64) DEFAULT '' NOT NULL,
				s_element varchar(64) DEFAULT '' NOT NULL,
				PRIMARY KEY  (id)
			) $charset_collate;
			CREATE TABLE {$wpdb->prefix}wpdsc_metadatavalue (
				id bigint(20) NOT NULL AUTO_INCREMENT,
				i_schema_id bigint(20) NOT NULL,
				i_items_id bigint(20) NOT NULL,
				s_text_value text DEFAULT '',
				PRIMARY KEY  (id),
				KEY metadata_id (i_schema_id),
				KEY i_items_id (i_items_id)
			) $charset_collate;
			CREATE TABLE {$wpdb->prefix}wpdsc_items (
				id bigint(20) NOT NULL AUTO_INCREMENT,
				s_handle varchar(180) DEFAULT '',
				i_origen_id bigint(20) NOT NULL,
				PRIMARY KEY  (id),
				KEY i_origen_id (i_origen_id)
			) $charset_collate;
			CREATE TABLE {$wpdb->prefix}wpdsc_bitstreams (
				id bigint(20) NOT NULL AUTO_INCREMENT,
				s_name varchar(180) DEFAULT '' NOT NULL,
				s_url text DEFAULT '' NOT NULL,
				s_format varchar(180) DEFAULT '' NOT NULL,
				i_sizebytes bigint(20) NOT NULL,
				i_items_id bigint(20) NOT NULL,
				s_bundle varchar(180) DEFAULT '' NOT NULL,
				PRIMARY KEY  (id),
				KEY i_items_id (i_items_id)
			) $charset_collate;
			CREATE TABLE {$wpdb->prefix}wpdsc_origen (
				id bigint(20) NOT NULL AUTO_INCREMENT,
				s_nombre text DEFAULT '',
				s_url text DEFAULT '',
				PRIMARY KEY  (id)
			) $charset_collate;";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );

			add_option( 'wpdsc_database_version', '1.0' );
		}
	}
	/*require_once(WPDSC_ROUTE . '/includes/functions.php');
    $funcion = new funciones();

	function wpdsc_cron(){
		if(! wp_next_scheduled('wpdsc_cron_job')){
			wp_scheduled_event(current_time('timestamp'),'hourly','wpdsc_cron_job');
		}
	}
	add_action('wpdsc_cron_job','wpdsc_cron_job_now');
	function wpdsc_cron_job_now(){
		$source = 'http://centroderecursos.educarchile.cl';
		$nombre = 'Centro de recursos, Educar Chile';
		$limit = 100;
		$funcion -> DeletedItem($source, $limit,0);
		error_log('Mi evento se ejecutó: '.Date("h:i:sa"));
	}*/