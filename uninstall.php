<?php
/*
	BORRADOR DE TABLAS CUANDO SE DESACTIVA EL PLUGIN (DE MOMENTO) EN DESARROLLO.
	EL FUNCIONAMIENTO DE ESTA FUNCIÓN SE IMPLEMETARÁ CUANDO SE DEBA DESINSTALAR.
*/

	function wpdsc_uninstall(){
        global $wpdb;
        $tableArray = [
          $wpdb->prefix . 'wpdsc_metadataschema',
          $wpdb->prefix . 'wpdsc_metadatavalue',
          $wpdb->prefix . 'wpdsc_items',
          $wpdb->prefix . 'wpdsc_origen',
					$wpdb->prefix . 'wpdsc_bitstreams'
       ];

      foreach ($tableArray as $tablename) {
         $wpdb->query("DROP TABLE IF EXISTS $tablename");
      }

      delete_option('wpdsc_database_version');
    }

    //register_uninstall_hook(__FILE__, 'delete_plugin_database_tables');
