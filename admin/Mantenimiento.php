<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>
    <body>
        <div>
            <hr>
            <div>
                Funciones: <br>
                <ul>
                    <li>
                        <h5>Delete items: </h5> 
                        Hace un listado con todos los items que se encuentren en el origen de los datos
                        (Repositorios o Bibliotecas digitales). y luego elimina de la base de datos los items que no se encuentren. <br>
                    </li>
                    <li>
                        <h5>Update items:</h5> 
                        Revisa la fecha de modificación del item dentro de Dspace, y luego en la base de datos elimina e inserta nuevamente el item actualizado. <br>
                    </li>
                    <li>
                        <h5>Insert new items:</h5> 
                        <span>Temporalmente fuera de servicio</span>
                    </li>
                </ul>
            </div>
            <hr>
            <div class="container">
                <form method="POST" action="?page=wp_dspaceplugin%2Fadmin%2FMantenimiento.php">
                    <div class="form-group row">
                        <label for="accion"> Selecciona una acción </label>
                        <select class="custom-select" name="evento" id="evento">
                            <option selected value="">Selecciona...</option>
                            <option value="delete">
                                Delete items
                            </option>
                            <option value="update">
                                Update items
                            </option>
                            <option value="insert">
                                Insert new items (f.s)
                            </option>
                        </select>
                    </div>
                    <div class="form-group row">
                        <!--label for="origen"> Selecciona un origen de datos</label>
                        <div>
                            <select class="custom-select mr-sm-2" name="origen" id="origen">
                                <option selected>Selecciona...</option>
                        <?php
                        /*global $wpdb;
                        $fuente = $wpdb->get_results("SELECT * FROM wp_wpdsc_origen");
                        foreach ($fuente as $key) {
                            echo '<option value="' . $key->s_url . '">' . $key->s_nombre . '</option>';
                        }*/
                        ?>
                            </select>
                        </div-->
                        <label class="col-xs-3 col-form-label mr-2">Ingresa fuente de datos</label>
                        <div class="col-xs-9">
                            <input type="text" name="url" id="url"
                                   value=<?php
                                   global $wpdb;
                                   $fuente = $wpdb->get_results("SELECT s_url FROM wp_wpdsc_origen");
                                   echo '"' . $fuente[0]->s_url . '"';
                                   ?>  />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="offset-xs-3 col-xs-9">
                            <input type="submit" value="Ejecutar" class="btn btn-primary" id="button" name="button"> 
                        </div>
                    </div>
                </form>
            </div>
            <hr>
        </div>
    </body>
</html>


<?php
// Esta clase sera el panel para realizar el resto de funciones "Update" y "Delete"

if (isset($_POST['button'])) {
    require_once(WPDSC_ROUTE . '/includes/function/Update.php');
    require_once(WPDSC_ROUTE . '/includes/function/Delete.php');
    require_once(WPDSC_ROUTE . '/includes/function/NewInsert.php');
    $option = $_POST['evento'];
    $origen = $_POST['url'];
    $offset = 0;
    $limit = 100;
    if (strpos($origen, 'odepa.gob.cl') !== false || strpos($origen, 'odepa.cl') !== false) {
    //if (strpos($origen, 'localhost:8080') !== false || strpos($origen, 'localhost:8080') !== false) {
        if ($option == 'update') {
            $update = new Update();
            $update->UpdateItems($origen, $offset, $limit);
        } elseif ($option == 'delete') {
            $delete = new Delete();
            $delete->DeletedItem($origen, $offset, $limit);
        } elseif ($option == 'insert') {
            $insert = new NewInsert();
            $insert->Insert($origen, $offset, $limit);
        }elseif ($option == ''){
            echo 'Indicar una acción';
        }
    } else {
        echo 'La url ingresada no corresponde al dominio de ODEPA';
    }
}
?>
<!--script type="text/javascript">
    function uri() {
        var url = document.getElementById("url").value;
        if(url.includes("odepa.gob.cl") !== false || url.includes('odepa.cl') !== false)){
        //if(url.includes("localhost:8080") !== false){
            
        }else{
            alert("La url no corresponde al dominio de Odepa");
        }
    }
</script-->