<?php
if (isset($_POST['cron'])) {
    $date = get_gmt_from_date(date('Y-m-d H:i:s', strtotime($_POST['time'])), 'U');
    $recu = $_POST['recurrence'];
    $origen = $_POST['url'];
    if (strpos($origen, 'odepa.gob.cl') !== false || strpos($origen, 'odepa.cl') !== false) {
        if ($_POST['action'] === 'wpdsc_delete') {
            $name = 'cron_delete';
            $cron = new Cron();
            $cron->cron_create($date, $recu, $name, $origen);
        } elseif ($_POST['action'] === 'wpdsc_update') {
            $name = 'cron_update';
            $cron = new Cron();
            $cron->cron_create($date, $recu, $name, $origen);
        } elseif ($_POST['action'] === 'wpdsc_insert') {
            $name = 'cron_insert';
            $cron = new Cron();
            $cron->cron_create($date, $recu, $name, $origen);
        }
    }else{
        echo '<h3>La url ingresada no corresponde al dominio de ODEPA</h3>';
    }
}

class Cron {

    public function cron_create($date, $recu, $name, $origen) {
        require_once(WPDSC_ROUTE . '/includes/function/Update.php');
        require_once(WPDSC_ROUTE . '/includes/function/Delete.php');
        require_once(WPDSC_ROUTE . '/includes/function/NewInsert.php');
        $offset = 0;
        $limit = 100;
        if (!wp_next_scheduled($name)) {
            wp_schedule_event($date, $recu, $name);
        }
        if ($name === 'cron_insert') {
            $insert = new NewInsert();
            $insert->Insert($origen, $offset, $limit);
        } elseif ($name === 'cron_delete') {
            add_action($name, $this->delete($origen));
        } elseif ($name === 'cron_update') {
            $update = new Update();
            $update->UpdateItems($origen, $offset, $limit);
        }
    }

    public function delete($origen) {
        global $wpdb;
        $items = array();
        $ae = 0;
        $ne = 0;
        $fuente = $wpdb->get_results("Select id from wp_wpdsc_origen where s_url = '" . $origen . "'");
        $cpi = $wpdb->get_results("SELECT s_handle FROM wp_wpdsc_items where i_origen_id =" . $fuente[0]->id);

        $enlace = file_get_contents($origen . '/rest/items/?expand=all');
        $json = json_decode($enlace);
        if ($json != null) {

            foreach ($json as $value) {
                array_push($items, $value->handle);
            }
            foreach ($cpi as $key) {
                if (in_array($key->s_handle, $items)) {
                    
                } else {
                    $item = $wpdb->get_results("SELECT id FROM wp_wpdsc_items where s_handle = '" . $key->s_handle . "'");
                    $wpdb->delete('wp_wpdsc_metadatavalue',
                            array('i_items_id' => $item[0]->id)
                    );
                    $wpdb->delete('wp_wpdsc_bitstreams',
                            array('i_items_id' => $item[0]->id)
                    );
                    $wpdb->delete('wp_wpdsc_items',
                            array('id' => $item[0]->id)
                    );
                }
            }
        }
    }

}
?>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <!--div>
            <a href="?page=wp_dspaceplugin/admin/configuration.php" class="a"><i class="fa fa-home" style="font-size:48px;"></i>Volver al inicio</a>
        </div-->
        <form action="?page=wp_dspaceplugin/admin/Cron.php" method="POST">
            <h4>Programar tarea</h4>
            <div>
                <label for="time">Fecha de inicio</label>
                <input type="text" name="time" id="time" placeholder="YYYY-MM-DD HH:MM:SS"><br>
            </div>
            <div>
                <label for="recurrence">
                    Periodo
                </label>
                <select name ="recurrence" id = "recurrence">
                    <option selected="">Periodo</option>
                    <?php
                    $json = json_encode(wp_get_schedules());
                    $decod = json_decode($json);
                    foreach ($decod as $key => $value) {
                        ?> 
                        <option value=<?php echo $key ?>>
                            <?php
                            foreach ($value as $lol) {
                                echo $lol . '  ';
                            }
                            ?></option><?php
                        }
                        ?>
                </select><br>
            </div>
            <div>
                <label for="action">Tarea</label>
                <select name="action" id="action">
                    <option value="wpdsc_update"> Actualizar ítems</option>
                    <option value="wpdsc_delete"> Eliminar ítems</option>
                    <option value="wpdsc_insert"> Insertar ítems</option>
                </select>
            </div>
            <div>
                <div>
                    <!--label for="origen">Fuente de datos: </label>
                    <select class="custom-select mr-sm-2" name="origen" id="origen">
                        <option selected>Fuente de datos</option>
                    <?php
                    /* global $wpdb;
                      $fuente = $wpdb->get_results("SELECT * FROM wp_wpdsc_origen");
                      foreach ($fuente as $key) {
                      echo '<option value="' . $key->s_url . '">' . $key->s_nombre . '</option>';
                      } */
                    ?>
                    </select-->
                    <label class="col-xs-3 col-form-label mr-2">Ingresa fuente de datos</label>
                    <div class="col-xs-9">
                        <input type="text" name="url" id="url" onfocusout="return uri()"
                               value=<?php
                    global $wpdb;
                    $fuente = $wpdb->get_results("SELECT s_url FROM wp_wpdsc_origen");
                    echo '"' . $fuente[0]->s_url . '"';
                    ?>  />
                    </div>
                </div>
            </div>
            <div>
                <input type="submit" name="cron" id="cron" value="Registrar">
            </div>
        </form>
    </body>
</html>
