
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<body>
    <div class="wrap">
        <h2><?php _e('WP DSpace connect', 'wp_dspace_connect') ?></h2>
        <fieldset>
            <legend><?php _e('Plugin configuration', 'wp_dspace_connect') ?></legend>
            <hr/>
            <p>
                <strong> Funciones: </strong> <br>
                1.- Registrar Schema de metadatos dc <br>
                2.- Registrar Origen de los datos<br>
                3.- Registrar items, indicar url de origen de datos. Si los datos de la lista ya estan registrados la función los detectara y no los volverá a guardar en la base de datos. <br>
                4.- Actualizar datos de un item. <br>
                5.- Borrar un item.
                <br><br>
            </p>
            <hr/>
            <div class="container">
                <form action="?page=conector-wp-dspace/admin/configuration.php" method="POST">
                    <div class="form-group row">
                        <label for="form_element_per_page" class="col-xs-3 col-form-label mr-2"> URL de fuente de datos</label>
                        <div class="col-xs-9">
                            <input type="text" class="form-control" 
                                   id="form_element_per_page" name="origen" id="origen"
                                   placeholder="http://localhost:8080">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="form_element_per_page_1" class="col-xs-3 col-form-label mr-2"> Nombre de fuente de datos</label><br>
                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="form_element_per_page_1" name="nombre" placeholder="Dspace local">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="offset-xs-3 col-xs-9">
                            <input type="submit" value="Guardar Schema y Origen de datos" class="btn btn-secondary" name='1'>
                            <input type="submit" value="Consumir e indexar" name="2" class="btn btn-secondary">
                        </div>
                    </div>
                </form>
            </div>
            <hr/>
        </fieldset>
    </div>
</body>

<p>
    <?php
    if (isset($_POST['1'])) {
        require_once(WPDSC_ROUTE . '/includes/function/InsertSchema.php');
        global $wpdb;


        $funcion = new funciones();

        $origen = $_POST['origen'];
        if (strpos($origen, '/', -1) !== false) {
            $origen = trim($origen, '/');
        }
        $nombre = $_POST['nombre'];
        if ($origen == '' && $nombre == '' || $origen == '' || $nombre == '') {
            echo 'Debe ingresar nombre y url de fuente de datos';
        } else {
            if (strpos($origen, 'odepa.gob.cl') !== false || strpos($origen, 'odepa.cl') !== false) {
                //if (strpos($origen, 'localhost:8080') !== false || strpos($origen, 'localhost:8080') !== false) {
                echo '<h4> Datos ingresados </h4>';
                echo '<strong>' . $origen . '</strong><br>';
                echo '<strong>' . $nombre . '</strong><br>';
                echo '<hr>';

                //FUNCION PARA INGRESAR SCHEMA DE METADATOS

                $consulta = $wpdb->get_results("SELECT COUNT(id) as total FROM wp_wpdsc_metadataschema");
                $funcion->InsertOrigen($origen, $nombre);
                if ($consulta[0]->total == 0) {
                    //FUNCION PARA INGRESAR SCHEMA DE METADATOS

                    $funcion->InsertSchema($origen);
                }
            } else {
                echo 'La url ingresada no corresponde al dominio de ODEPA';
            }
        }
    } elseif (isset($_POST['2'])) {
        require_once(WPDSC_ROUTE . '/includes/function/InsertItem.php');
        $funcion = new Items();
        $origen = $_POST['url'];
        //$origen = $_POST['url'];
        /* $nombre = $_POST['nombre']; */
        $limit = 100;
        if ($origen == '') {
            echo 'Debe ingresar al menos url de origen de datos';
        } else {
            if (strpos($origen, 'odepa.gob.cl') !== false || strpos($origen, 'odepa.cl') !== false) {
                //if (strpos($origen, 'localhost:8080') !== false || strpos($origen, 'localhost:8080') !== false) {
                echo 'Indexando registros desde: ' . $origen;
                //Comprobar si existe el item e ingresar
                $funcion->InsertItems($origen, $limit, 0);
            } else {
                echo 'La url ingresada no corresponde al dominio de ODEPA';
            }
        }
    }


    global $wpdb;
    $fuente = $wpdb->get_results("SELECT * FROM wp_wpdsc_origen");
    ?>
<h4> Orígenes de datos registrados</h4>
<form action="?page=wp_dspaceplugin/admin/configuration.php" method="POST">
    <table class="table">
        <thead>
            <tr>
                <!--th scope="col">#</th-->
                <th scope="col">Url</th>
                <th scope="col">Nombre</th>
                <th scope="col">Acción</th>
            </tr>
        </thead>
        <tbody>

            <?php
            $cont = 1;
            foreach ($fuente as $key) {
                echo '<tr>';
                //echo '<th scope="row">'.$cont.'</th>';
                echo '<td><input type=hidden name=url id=url value="' . $key->s_url . '">' . $key->s_url . '</td>';
                echo '<td>' . $key->s_nombre . '</td>';
                echo '<td><input type="submit" value="Consumir e indexar" name="2" class="btn btn-secondary"></td>';
                echo '</tr>';
                $cont++;
            }
            ?>
        </tbody>
    </table>
</form><br>
<?php
//$funcion -> InsertItems($source,$limit,0);
?>
<hr>
<?php ?>

