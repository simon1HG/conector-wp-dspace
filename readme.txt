Pre requisitos:
	Wordpress: 
		Versión->

Instalación del plugin:

Hace un git clone al repositorio dento de la carpeta plugins de wordpress.
La ruta es: 
ruta de wordpress\wp-content\plugins

Esto habilitará inmediatamente el conector en la sección plugins dentro del panel de administración de wordpress,
dejando listo para la activación del plugin.


Activación del plugin:

Al activar el plugin, en la base de datos se crearán 5 tablas nuevas con un prefijo 'wp_wpdsc', que no influyen el esquema original
de la base de datos. 

NOTA: Al desactivar el plugin estas tablas se eliminan las tablas y toda la información en esta.

Además de crear las tablas, el plugin crea un Menú en el panel lateral de wordpress para poder hacer de su uso.

