<?php
class funciones {
  public function InsertSchema($url){
    $schema = file_get_contents($url.'/rest/registries/schema/dc');
    $json = json_decode($schema);
    global $wpdb;
    foreach ($fields = $json -> metadataFields as $key => $value ){
      foreach ($value as $k => $v) {
        $sdb = $wpdb->get_results("SELECT s_element FROM wp_wpdsc_metadataschema WHERE s_element = '".$v."'");
        if($sdb[0] -> s_element != ''){
          echo 'Ya esta registrado <br>';
        }else{
          if($k == 'name'){
            $dc = $v;
            $array = explode(".",$dc);
            if (sizeof($array) == 3){
              $wpdb->INSERT('wp_wpdsc_metadataschema',
                array(
                  's_schema' => $array[0],
                  's_element' => $array[0].".".$array[1].".".$array[2]
                )
              );
            }else if(sizeof($array) == 2){
              $wpdb->INSERT('wp_wpdsc_metadataschema',
                array(
                  's_schema' => $array[0],
                  's_element' => $array[0].".".$array[1]
                )
              );
            }
          }
        }
      }
    }
    $lf = $wpdb -> get_results('SELECT s_element FROM wp_wpdsc_metadataschema WHERE s_element = "dc.date.lastmodification"');
    if($lf[0] -> s_element != ''){
      echo 'Ya existe';
    }else{
      $wpdb->INSERT('wp_wpdsc_metadataschema',
        array(
          's_schema' => 'dc',
          's_element' => 'dc.date.lastmodification'
        )
    );
    }
  }
  public function InsertItems($url,$limit){
    global $wpdb;
    if($limit == null){
        $enlace = file_get_contents($url.'/rest/items/?expand=all&offset=0');
      }else{
        $enlace = file_get_contents($url.'/rest/items/?expand=all&offset=0&limit='.$limit);
      }
    $json = json_decode($enlace);
    foreach ($json as $value) {
      $fuente = $wpdb->get_results("Select id from wp_wpdsc_origen where s_url = '".$url."'");
      $item = $wpdb->get_results("SELECT s_handle FROM wp_wpdsc_items where s_handle = '".$value -> handle."' and i_origen_id =".$fuente[0] -> id);

      $lm = $wpdb->get_results("SELECT id FROM wp_wpdsc_metadataschema WHERE s_element = 'dc.date.lastmodification'");
      if($item[0] -> s_handle != ''){
      }else{
        $wpdb->INSERT('wp_wpdsc_items',
          array(
            's_handle' => $value -> handle,
            'i_origen_id' => $fuente[0] -> id
          )
        );
        $item = $wpdb->get_results("Select id from wp_wpdsc_items where s_handle = '".$value -> handle."'");
        $wpdb->INSERT('wp_wpdsc_metadatavalue',
          array(
          'i_schema_id' => $lm[0] -> id,
          'i_items_id' => $item[0] -> id,
          's_text_value' => $value -> lastModified
          )
        );

        foreach ($value -> metadata as $metadata) {
          $mid = $wpdb->get_results("Select id, s_element from wp_wpdsc_metadataschema where s_element = '".$metadata -> key."'");
          $wpdb->INSERT('wp_wpdsc_metadatavalue',
            array(
              'i_schema_id' => $mid[0] -> id,
              'i_items_id' => $item[0] -> id,
              's_text_value' =>$metadata -> value
            )
          );
        }
        foreach ($value -> bitstreams as $bitstreams) {
          global $wpdb;
          $wpdb->INSERT('wp_wpdsc_bitstreams',
            array(
              's_name' => $bitstreams -> name,
              's_url' => $bitstreams -> retrieveLink,
              's_format' => $bitstreams -> format,
              'i_sizebytes' => $bitstreams -> sizeBytes,
              'i_items_id' => $item[0] -> id,
              's_bundle' => $bitstreams -> bundleName,
            )
          );
        }
      }
    }
  }
  public  function InsertOrigen($url,$nombre){
      global $wpdb;
      $vf = $wpdb->get_results("select count(id) as id from wp_wpdsc_origen where s_url = '".$url."'");
      if(($vf[0] -> id) == 0){
        $wpdb->INSERT('wp_wpdsc_origen',
        array(
          's_nombre' => $nombre,
          's_url' => $url
        )
      );
    }
  }
  /*public function UpdateItems($origen,$limit){
    global $wpdb;
      if($limit == null){
        $enlace = file_get_contents($origen.'/rest/items/?expand=all&offset=0');
      }else{
        $enlace = file_get_contents($origen.'/rest/items/?expand=all&offset=0&limit='.$limit);
      }
    
    $url = $wpdb->get_results("select id as id from wp_wpdsc_origen where s_url = '".$origen."'");
    $json = json_decode($enlace);
    foreach ($json as $value) {
      $item = $wpdb->get_results("SELECT s_handle FROM wp_wpdsc_items where s_handle = '".$value -> handle."' and i_origen_id =".$url[0] -> id);
      if($item[0] -> s_handle == ''){
        $this -> InsertItems($origen,$limit);
      }else{
        foreach ($value -> metadata as $metadata) {
          $mdbd = $wpdb->get_results("SELECT s_text_value from wp_wpdsc_metadatavalue mv, wp_wpdsc_metadataschema ms where i_items_id = (SELECT i.id FROM wp_wpdsc_items i, wp_wpdsc_origen o WHERE i.i_origen_id = o.id and i.s_handle = '".$item[0] -> s_handle."') and mv.i_schema_id = ms.id and s_text_value = '".$metadata -> value."'");
          if($mdbd[0]-> s_text_value == ''){
            echo $metadata -> value.'<br>';
          }else{
            if($mdbd[0]-> s_text_value != $metadata -> value){
              echo 'Cambio TODO :O';
            }
          }
        }
        foreach ($value -> bitstreams as $bitstreams) {
          $bsbd = $wpdb->get_results("SELECT s_nombre from wp_wpdsc_bitstreams where i_items_id = (SELECT i.id 
                    FROM wp_wpdsc_items i, wp_wpdsc_origen o 
                    WHERE i.i_origen_id = o.id and i.s_handle = '".$item[0]-> s_handle."') 
                    and s_nombre = '".$bitstreams -> name."'");
          if($bsbd[0]-> s_nombre == ''){
            echo '<strong>'.$bitstreams -> name.'</strong><br>';
          }else{
            echo $bsbd[0] -> s_nombre.'<br>';
          }
        }
      }
    }
  }*/
}

