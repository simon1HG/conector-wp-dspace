<?php

function wpdsc_menu_administrador() {
    add_menu_page('WP-DSpace connect', 'WP-DSpace connect', 'manage_options', WPDSC_ROUTE . '/admin/configuration.php');
    add_submenu_page(WPDSC_ROUTE . '/admin/configuration.php', 'Mantenimiento', 'Mantenimiento', 'manage_options', WPDSC_ROUTE . '/admin/Mantenimiento.php');
    add_submenu_page(WPDSC_ROUTE . '/admin/configuration.php', 'Cron options', 'Cron options', 'manage_options', WPDSC_ROUTE . '/admin/Cron.php');
}

add_action('admin_menu', 'wpdsc_menu_administrador');
