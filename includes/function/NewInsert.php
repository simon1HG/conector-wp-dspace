<?php

class NewInsert {

    public function Insert($url, $os, $limit) {
        global $wpdb;
        $fuente = $wpdb->get_results("Select id from wp_wpdsc_origen where s_url = '" . $url . "'");
        $enlace = file_get_contents($url . '/rest/items/?expand=all&offset=' . $os . '&limit=' . $limit);
        $json = json_decode($enlace);
        if ($json != null && $json != "[]") {
            foreach ($json as $value) {
                $item = $wpdb->get_results("SELECT s_handle FROM wp_wpdsc_items where s_handle = '" . $value->handle . "' and i_origen_id =" . $fuente[0]->id);
                $lm = $wpdb->get_results("SELECT id FROM wp_wpdsc_metadataschema WHERE s_element = 'dc.date.lastmodification'");
                if (@$item[0]->s_handle != '') {
                    
                } else {
                    $wpdb->INSERT('wp_wpdsc_items',
                            array(
                                's_handle' => $value->handle,
                                'i_origen_id' => $fuente[0]->id
                            )
                    );
                    $item = $wpdb->get_results("Select id from wp_wpdsc_items where s_handle = '" . $value->handle . "'");
                    $wpdb->INSERT('wp_wpdsc_metadatavalue',
                            array(
                                'i_schema_id' => $lm[0]->id,
                                'i_items_id' => $item[0]->id,
                                's_text_value' => $value->lastModified
                            )
                    );

                    foreach ($value->metadata as $metadata) {
                        $mid = $wpdb->get_results("Select id, s_element from wp_wpdsc_metadataschema where s_element = '" . $metadata->key . "'");
                        $wpdb->INSERT('wp_wpdsc_metadatavalue',
                                array(
                                    'i_schema_id' => $mid[0]->id,
                                    'i_items_id' => $item[0]->id,
                                    's_text_value' => $metadata->value
                                )
                        );
                    }
                    foreach ($value->bitstreams as $bitstreams) {
                        global $wpdb;
                        $wpdb->INSERT('wp_wpdsc_bitstreams',
                                array(
                                    's_name' => $bitstreams->name,
                                    's_url' => $bitstreams->retrieveLink,
                                    's_format' => $bitstreams->format,
                                    'i_sizebytes' => $bitstreams->sizeBytes,
                                    'i_items_id' => $item[0]->id,
                                    's_bundle' => $bitstreams->bundleName,
                                )
                        );
                    }
                }
            }
            $ofs = $os + $limit;
            $this->Insert($url, $ofs, $limit);
        }
    }

}
