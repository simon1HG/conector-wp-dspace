<?php

class Delete {

    function DeletedItem($origen,$offset,$limit) {
        global $wpdb;

        $items = array();
        $ae = 0;
        $ne = 0;
        $fuente = $wpdb->get_results("Select id from wp_wpdsc_origen where s_url = '" . $origen . "'");
        $cpi = $wpdb->get_results("SELECT s_handle FROM wp_wpdsc_items where i_origen_id =" . $fuente[0]->id);

        $enlace = file_get_contents($origen . '/rest/items/?expand=all' . $offset . '&limit=' . $limit);
        $json = json_decode($enlace);
        if($json != null && $json != "[]") {
            foreach ($json as $value) {
                array_push($items, $value->handle);
            }
            foreach ($cpi as $key) {
                if (in_array($key->s_handle, $items)) {
                    $ne++;
                } else {
                    //si no existe el item se elimina de la base de datos
                    $item = $wpdb->get_results("SELECT id FROM wp_wpdsc_items where s_handle = '" . $key->s_handle . "'");
                    $wpdb->delete('wp_wpdsc_metadatavalue',
                            array('i_items_id' => $item[0]->id)
                    );
                    $wpdb->delete('wp_wpdsc_bitstreams',
                            array('i_items_id' => $item[0]->id)
                    );
                    $wpdb->delete('wp_wpdsc_items',
                            array('id' => $item[0]->id)
                    );
                    $ae++;
                }
            }
            $offset = $offset + $limit;
            $this->DeletedItem($origen,$offset,$limit);
        }

    }

}
