<?php

class Update {

    public function UpdateItems($origen,$offset,$limit) {
        global $wpdb;
        $enlace = file_get_contents($origen . '/rest/items?expand=all&offset=' . $offset);
        $json = json_decode($enlace);
        $ia = 0;
        $ina = 0;
        if ($json != null && $json != "[]") {
            foreach ($json as $value) {
                $fuente = $wpdb->get_results("Select id from wp_wpdsc_origen where s_url = '" . $origen . "'");
                $cpi = $wpdb->get_results("SELECT s_handle FROM wp_wpdsc_items where s_handle = '" . $value->handle . "' and i_origen_id =" . $fuente[0]->id);
                if (@$cpi[0]->s_handle != '') {
                    $fbd = $wpdb->get_results("SELECT id FROM wp_wpdsc_items where s_handle = '" . $cpi[0]->s_handle . "'");
                    $lm = $wpdb->get_results("SELECT id FROM wp_wpdsc_metadataschema where s_element = 'dc.date.lastmodification'");
                    $ilm = $wpdb->get_results("SELECT s_text_value FROM `wp_wpdsc_metadatavalue` where i_items_id =" . $fbd[0]->id . " and i_schema_id =" . $lm[0]->id);
                    if ($ilm[0]->s_text_value != $value->lastModified) {

                        $func = new Update();
                        $func->DeleteHandle($value->handle, $origen);
                        $func->InsertUp($origen, $origen . $value->link);
                        $ia++;
                    }
                }
            }
            $offset = $offset + $limit;
            $this->UpdateItems($origen,$offset,$limit);
        }

    }

    public function InsertUp($origen, $url) {
        global $wpdb;
        $fuente = $wpdb->get_results("Select id from wp_wpdsc_origen where s_url = '" . $origen . "'");
        $enlace = file_get_contents($url . '/?expand=all');
        $json = json_decode($enlace);
        $lm = $wpdb->get_results("SELECT id FROM wp_wpdsc_metadataschema WHERE s_element = 'dc.date.lastmodification'");

        $wpdb->INSERT('wp_wpdsc_items',
                array(
                    's_handle' => $json->handle,
                    'i_origen_id' => $fuente[0]->id
                )
        );
        $id = $wpdb->get_results("SELECT id FROM wp_wpdsc_items where s_handle = '" . $json->handle . "' and i_origen_id =" . $fuente[0]->id);
        $wpdb->INSERT('wp_wpdsc_metadatavalue',
                array(
                    'i_schema_id' => $lm[0]->id,
                    'i_items_id' => $id[0]->id,
                    's_text_value' => $json->lastModified
                )
        );
        foreach ($json->metadata as $metadata) {
            $mid = $wpdb->get_results("Select id, s_element from wp_wpdsc_metadataschema where s_element = '" . $metadata->key . "'");
            $wpdb->INSERT('wp_wpdsc_metadatavalue',
                    array(
                        'i_schema_id' => $mid[0]->id,
                        'i_items_id' => $id[0]->id,
                        's_text_value' => $metadata->value
                    )
            );
        }
        foreach ($json->bitstreams as $bitstreams) {
            global $wpdb;
            $wpdb->INSERT('wp_wpdsc_bitstreams',
                    array(
                        's_name' => $bitstreams->name,
                        's_url' => $bitstreams->retrieveLink,
                        's_format' => $bitstreams->format,
                        'i_sizebytes' => $bitstreams->sizeBytes,
                        'i_items_id' => $id[0]->id,
                        's_bundle' => $bitstreams->bundleName,
                    )
            );
        }
    }

    public function DeleteHandle($handle, $url) {
        global $wpdb;
        $fuente = $wpdb->get_results("Select id from wp_wpdsc_origen where s_url = '" . $url . "'");
        $item = $wpdb->get_results("SELECT id FROM wp_wpdsc_items where s_handle = '" . $handle . "' and i_origen_id =" . $fuente[0]->id);
        $wpdb->delete('wp_wpdsc_metadatavalue',
                array('i_items_id' => $item[0]->id)
        );
        $wpdb->delete('wp_wpdsc_bitstreams',
                array('i_items_id' => $item[0]->id)
        );
        $wpdb->delete('wp_wpdsc_items',
                array('id' => $item[0]->id)
        );
    }

}
