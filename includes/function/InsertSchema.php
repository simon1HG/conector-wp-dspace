<?php

class funciones {

    public function InsertSchema($url) {
        $schema = file_get_contents($url . '/rest/registries/schema/dc');
        $json = json_decode($schema);
        global $wpdb;
        foreach ($fields = $json->metadataFields as $key => $value) {
            foreach ($value as $k => $v) {
                @$sdb = $wpdb->get_results("SELECT s_element FROM wp_wpdsc_metadataschema WHERE s_element = '" . $v . "'");
                if (@$sdb[0]->s_element != '') {
                    //echo 'Ya esta registrado <br>';
                } else {
                    if ($k == 'name') {
                        $dc = $v;
                        $array = explode(".", $dc);
                        if (sizeof($array) == 3) {
                            $wpdb->INSERT('wp_wpdsc_metadataschema',
                                    array(
                                        's_schema' => $array[0],
                                        's_element' => $array[0] . "." . $array[1] . "." . $array[2]
                                    )
                            );
                        } else if (sizeof($array) == 2) {
                            $wpdb->INSERT('wp_wpdsc_metadataschema',
                                    array(
                                        's_schema' => $array[0],
                                        's_element' => $array[0] . "." . $array[1]
                                    )
                            );
                        }
                    }
                }
            }
        }
        $lf = $wpdb->get_results('SELECT s_element FROM wp_wpdsc_metadataschema WHERE s_element = "dc.date.lastmodification"');
        if (@$lf[0]->s_element != '') {
            //echo 'Ya existe';
        } else {
            $wpdb->INSERT('wp_wpdsc_metadataschema',
                    array(
                        's_schema' => 'dc',
                        's_element' => 'dc.date.lastmodification'
                    )
            );
        }
    }

    public function InsertOrigen($url, $nombre) {
        global $wpdb;
        $vf = $wpdb->get_results("select count(id) as id from wp_wpdsc_origen where s_url = '" . $url . "'");
        if (($vf[0]->id) == 0) {
            $wpdb->INSERT('wp_wpdsc_origen',
                    array(
                        's_nombre' => $nombre,
                        's_url' => $url
                    )
            );
        }
    }

}
